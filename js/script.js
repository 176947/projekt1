// function mojaFunkcja(){
//     kolory=["yellow","red","blue"]
//     document.getElementById("zmianakoloru").style.backgroundColor="blue";
// }
// document.write("Hello world");
//     console.log("trololo");
//     window.alert("siema");
//     alert("Hej");
var kolory = ["blue", "red", "green", "yellow"]; // tablica kolorów do zmiany

var aktualnyKolorIndex = 0; // indeks aktualnie wybranego koloru

function mojaFunkcja() {
    // Pobierz element o id "zmianakoloru"
    var zmianaKoloruElement = document.getElementById("zmianakoloru");
    
    // Ustaw tło na kolejny kolor z tablicy
    zmianaKoloruElement.style.backgroundColor = kolory[aktualnyKolorIndex];
    
    // Zwiększ indeks aktualnego koloru, a jeśli przekroczy długość tablicy, wróć do początku
    aktualnyKolorIndex = (aktualnyKolorIndex + 1) % kolory.length;
}